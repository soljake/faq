# Administrator Quick Start Guide

## Introduction
Welcome to the iTracR Manager Approvals Guide. This document is intended to guide you, the organisation administrator, through a brief demonstration of the new iTracR manager approvals system.

### Setting a user's manager
The Users page shows a summary of all users (both internal staff and external suppliers) with their accompanying job titles, their roles, availability/annual leave status, last timecard entry and last login information on iTracR.

You can assign a manager to a user from the edit user page. Click on the "Edit" button to the right to proceed to the edit user page.

On the right hand side of the edit user page, you will find a field captioned ‘Select a Manager’, this can be used to allocate a manager to the chosen user.
From here you can then choose whether the chosen manager will be able to approve time card approvals, leave approvals, or both.

### What the manager see's
Once a manager has been assigned, and once the manager has requests to approve. A "Management" button will appear on the navbar for the manager in the time card portal.

From here the manager can see the details of any submission needing managerial approval. It is important to note that all financial information other than expense details are not shown.

From this page, the manager is able to Approve or Reject an approval and can also attach a note to the request for the admin to see if appropriate.

If a decision has been made too hastily, it is possible to click ‘Undo’ afterwards and set the approval back to Pending while the appropriate action is decided upon.

Once approved, a green tick, will appear to the right of the approval. Similarly, a red cross will show if the approval is rejected. If you hover the mouse over the icon and a note is attached, it will show up as tooltip.

### What the administrator sees
On the Internal Approvals page, where you would normally view Time card and Invoice submissions, for time cards and invoices submitted, there will appear confirmation icons on the right.

If the submitted time card or invoice belongs to a user who has a manager, then one of following icons will be visible:
* A green tick for when a manager has approved the time card
* A question mark for when the manager is yet to make a decision
* A cross for when the manager has rejected the time card
* A circle-backslash symbol for users that do not have a manager

If the manager has attached a note to the approval, it can be seen by hovering over the icon.

Managers never have the final say on an approval. This functionality allows managers to peer review their staff's approvals and give financial administrators more assurance when dealing with approvals.
