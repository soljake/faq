# Administrator Quick Start Guide

## Introduction

Welcome to the itracr Quick Start Guide. This document is intended to guide you, the organisation administrator, through a brief demonstration of the itracr system.

It is important that you are familiar with the key processes of iTracR, these are:
* Adding Users (Internal Users / Supplier Users)
* Adding Clients
* Adding a Project
* Adding Tasks to the Project
* Allocating Users to the Tasks
* Reviewing Approvals

### Adding Users

The Users page shows a summary of all users (both internal staff and external suppliers) with their accompanying job titles, their roles, availability/annual leave status, last timecard entry and last login information on iTracR.

#### Adding a User
The side panel offers two options for creating a new user: you can choose to add either an internal user (a company employee), or a supplier user (typically someone who is contracted through another company). You can also view/edit existing users from the list.

Each action will direct you to a form in which to complete or update a user’s contact information, their email address and password, and to assign the user their iTracR
permissions. This allows you to choose whether to grant privileges beyond the standard functionality to complete company or supplier timecards

> NOTE: When adding a new supplier user, you must ensure that the company they work for (the supplier) has been created on the system first through Users > Suppliers > Add Supplier.

Here you can also add a reference number and any notes that you you will be able to set their default charging information (this can be changed per project/task). Supplier users will also include options to allow invoice generation and visible daily rate.

### Adding Clients
Before you can add projects and tasks for users to book their time against, you must create a client. This can be an external organisation or an internal department. Once on the Clients page, click ‘Add client’ (shown below) to create a new client.

Fill in the details of a client as shown in the image below, and ensure you save before proceeding. Once you have created a new client you will be able to add projects for that client directly through this screen from the ‘Client projects’ panel or by navigating to the Projects page explained in the next section. You can also add contacts associated with the client within the ‘Client contacts’ panel.

### Adding Projects
The Projects page shows a summary of all projects on the system. The table lists the client that each project is set against, the project types (pricing method), their start/end dates and the number of active tasks that each project contains. Clicking on a project name will direct you to a new screen in which you can view or amend the project details. To add a new project click on the "Add Project" button at the top of the page.

The Projects page shows a summary of all projects on the system. The table lists the client that each project is set against, the project types (pricing method), their start/end dates and the number of active tasks that each project contains. Clicking on a project name will direct you to a new screen in which you can view or amend the project details. To add a new project click on the Add Project button

>NOTE: To add a new Project you must first have a client who is the customer of the project through Clients > Add client.

Once you have added all the required information to create a project you can move onto adding Project Tasks and Client Contacts for that project.

### Adding Tasks
Once a Project is saved you are able to add Tasks and Client Contacts. The ‘Project tasks’ panel on the Project page displays a financial breakdown for the individual tasks within the project. To add a task to the newly created project page click the Add Task button in the ‘Project tasks’ panel (shown below). Once a task has been created you can also click on a task name to review the task details, allocate resources and update the task status.

Fill in the form with available information to create a new task. These details can be updated/edited at a later date. The ‘Task status’ panel keeps track of status changes made to the task and which user updated it. If the ‘Display on timecard’ option is selected any allocated users will see the task on their timecard (Task status must be Started to appear).

### Allocating Users to a Task
To assign a user to the newly created task select the ‘Add user’ button within the ‘User allocation’ panel (shown below). This panel will display the name of allocated users, their cost, charge rate and days booked against days assigned.

When adding a user to the task a panel will appear with the input fields and user details. You must select a user from the dropdown menu which will populate the form with the user’s default cost, charge and charging basis. It will also display the working hours per day and whether the user can book over this amount.

In this panel you can can change cost and charging details for this specific project without affecting any of the user’s other projects/tasks.

### Activation a Tasks
By default a task will start in the “Not Started” state and also set to not be displayed on a user’s timecard. To activate the task simply Update the status to “Started” and toggle the Display on user timecards option to “ON”.

### Reviewing Approvals
Approvals are categorised as internal approvals (time cards or invoices which have been submitted by employees or suppliers) or invoices for client approval (outgoing invoices that are being billed to a client). Both options are available from the iTracR menu under Approvals, along with the ‘edit time cards’ functionality.

##### Internal Approvals
This screen shows a list of all the internally submitted invoices and time cards alongside notice of the user they are related to, the date of submission, the invoice amount and the status. You are also able to view previous submissions which have already been approved or paid and are therefore closed.

Clicking ‘view’ will provide a breakdown of the time card or invoice awaiting action, including the hours booked and the costs attributed to each task during the period stated. Once saved, any changes to the status will be logged in the status history for each approval.

##### Invoices for Client Approvals
Any invoices generated from a task and submitted for client approval will appear as an outgoing invoice on this page with its accompanying details. You can choose to see both unpaid and paid invoices; you will also be able to view these invoices in PDF and HTML format by selecting the corresponding icons.

Updating the status for outgoing invoices is a similar process to updating the approval status for internally submitted invoices, with the same list of drop down options to choose from and access to a status log.

##### Holiday Approvals
Once a holiday is requested by a user, it is then shown in the ‘approvals’ page as ‘annual leave’. As you can see from the image you are able to see what user has requested holidays on their timesheet.

Once you’ve clicked on the user's name it will show what days they have requested off.

The approval page will show what dates have been requested and their current status update. It will also display any other requests which have been made previously before. As an admin you are able to ‘approve’ or ‘reject’ the users requests . This will then be saved on the user's timecard where they will be able to view the outcome.

##### Edit Time Cards
As an iTracR administrator, you also have the authority to edit any user’s time card. Select a username from the drop down list to receive access to their time card. From here you can see all hours/expenses and which date they were submitted on.

Should you need to change anything on the timecard you must first query that particular approval. This can be done by clicking the “Query” link below the desired date which will query all days associated with that particular approval. Alternatively, you can also click “Set timecard to queried” and select which approval you wish to query.
